/* class settings */
class Settings{
  /* constructor */
  constructor(){
    this.setupDefaults();
  }


  /* setup defaults */
  setupDefaults(){
    this.plr=true;                     // true:p1(b),false:p2(w)
    this.ovr=false;                    // game over status
    this.grid=new Array();             // the grid
    this.rows=3;                       // this.SET.rows
    this.cols=this.rows;               // this.cols
    this.dbug=false;                   // debug mode
    this.nln=String.fromCharCode(10);  // line break
    this.markerWhite='O';              // board marker
    this.markerBlack='X';              // board marker
    this.markerValid='+';              // board marker
    this.markerEmpty=' ';              // board marker
    this.cmp=true;                     // computer opponent?
    this.lvl=1;                        // cleverness (0:random,1:defensive,2:aggressive)
    this.dng=true;                     // allow diagonal calculations
    this.stwin=this.rows;              // how many stones to win
    this.btnaud='';                    // button audio
    this.clkaud='';                    // color button audio
    this.erraud='';                    // error audio
  }
}

/* class tools */
class Tools{
  /* constructor */
  constructor(set){
    this.SET=set;
  }


  /* add class to element */
  addClassToElementById(elmid,elmcls){
    var cls=this.getClassStringForElementById(elmid);
    if(cls.includes(elmcls)==false){
      var newcls=cls.concat(' '+elmcls);
      document.getElementById(elmid).className=newcls;
    }
  }

  /* remove class from element */
  removeClassFromElementById(elmid,elmcls){
    var cls=this.getClassStringForElementById(elmid);
    var newcls=cls.split(elmcls).join('');
    document.getElementById(elmid).className=newcls;
  }

  /* generate box id for click */
  getBoxIdForRowAndCol(r,c){
    return 'box-'+r+'-'+c;
  }

  /* get the string with class names for element */
  getClassStringForElementById(elmid){
    return document.getElementById(elmid).className;
  }

  /* do click on element by id */
  clickOnElementById(elmid){
    document.getElementById(elmid).click();
  }

  /* hide cover layer */
  hideCover(){
    this.addClassToElementById('cover1','cover1hide');
    this.addClassToElementById('cover2','cover2hide');
  }

  /* center game */
  centerWrapper(){
    var left=Math.floor((document.documentElement.clientWidth-document.getElementById('wrapper').offsetWidth)/2);
    document.getElementById('wrapper').style.marginLeft=left+'px';
    var top=Math.floor((document.documentElement.clientHeight-document.getElementById('wrapper').offsetHeight)/2);
    document.getElementById('wrapper').style.marginTop=top+'px';
    var btn=document.getElementById('new').offsetWidth;
    var left=Math.floor((document.getElementById('wrapper').offsetWidth-(btn*3))/2)-18;
    document.getElementById('info').style.marginLeft=left+'px';
  }

  /* remove item from array by key */
  removeItemFromArrayByKey(key,arr){
    arr.splice(key,1);
    return arr;
  }

  /* add item to array */
  addItemToArray(item,arr){
    arr.push(item);
    return arr;
  }

  /* play sound */
  playSound(snd){
    snd.cloneNode(true).play();
  }
}

class Sound{
  /* constructor */
  constructor(){
    this.setupAudio();
  }


  /* setup game audio files */
  setupAudio(){
    this.btnaud=new Audio('button.wav');
    this.btnaud.volume=0.15;
    this.clkaud=new Audio('click.wav');
    this.clkaud.volume=0.5;
    this.erraud=new Audio('error.wav');
    this.erraud.volume=0.5;
  }
}

/* class Game */
class Game{
  /* constructor */
  constructor(){
    // objects
    this.SET=new Settings();
    this.TLS=new Tools(this.SET);
    this.SND=new Sound();
    // event listener
    addEventListener('click',this);
    addEventListener('keydown',this);
    addEventListener('resize',this);
  }


  /* get start and end position in string for regex */
  getPositionForRegEx(str,regex,dir='>') {
    // setup
    dir=dir.toLowerCase();
    var pos=-1;
    var rex=new RegExp(regex);
    var match=rex.exec(str);
    // do we have a match and wht to do with it
    if(match!==null){
      switch(dir){
        case 'x':
          pos=match.index+1;
          break;
        case '<':
          pos=(match.index+match[0].length)-1;
          break;
        case '>':
        default:
          pos=match.index;
          break;
      }
    }
    // return
    return pos;
  }

  /* search for regex in grid */
  getPosForRegExSearchInGrid(regex,dir='>'){
    var pos=-1;
    var crd='';
    // horizontal
    for(var r=0;r<this.SET.rows;r++){
      var line='';
      for(var c=0;c<this.SET.cols;c++){line+=this.SET.grid[r][c];}
      var pos=this.getPositionForRegEx(line,regex,dir);
      if(pos>=0){crd=r+','+pos;break;}
    }
    // vertical
    if(crd==''){
      for(var c=0;c<this.SET.cols;c++){
        var line='';
        for(var r=0;r<this.SET.rows;r++){line+=this.SET.grid[r][c];}
        var pos=this.getPositionForRegEx(line,regex,dir);
        if(pos>=0){crd=pos+','+c;break;}
      }
    }
    // diagonal left-right-\
    if(this.SET.dng==true&&crd==''){
      var pr=0;
      var pc=this.SET.cols-1;
      while(pr<this.SET.rows){
        var p=0;
        var line='';
        while(parseInt(pc+p)<this.SET.cols&&parseInt(pr+p)<this.SET.rows){
          line+=this.SET.grid[parseInt(pr+p)][parseInt(pc+p)];p++;
        }
        var pos=this.getPositionForRegEx(line,regex,dir);
        if(pos>=0){crd=(pr+pos)+','+(pc+pos);break;}
        // count
        if(pc>-1){pc--;}
        if(pc==-1){pc=0;pr++;}
      }
    }
    // diagonal top-down-/
    if(this.SET.dng==true&&crd==''){
      var pr=0;
      var pc=0;
      while(pc<this.SET.cols){
        var p=0;
        var line='';
        while(parseInt(pc+p)<this.SET.cols&&parseInt(pr-p)>=0){
          line+=this.SET.grid[parseInt(pr-p)][parseInt(pc+p)];p++;
        }
        var pos=this.getPositionForRegEx(line,regex,dir);
        if(pos>=0){crd=(pr-pos)+','+(pc+pos);break;}
        // count
        if(pr<this.SET.rows){pr++;}
        if(pr==this.SET.rows){pr=this.SET.rows-1;pc++;}
      }
    }
    // return
    return crd;
  }



  /* get marker for player */
  getMarkerForPlayer(plrflg){
    if(plrflg){return this.SET.markerBlack;}
    return this.SET.markerWhite;
  }

  /* get color name for player */
  getColorNameForPlayer(plrflg){
    if(plrflg){return 'black';}
    return 'white';
  }

  /* minimum stones to block or go more aggressive */
  getThresholdForLevel(){
    return parseInt(this.SET.stwin-1);
  }

  /* toggle player */
  togglePlayer(){
    // switch and set color for player
    this.SET.plr=!this.SET.plr;
    // set X/O
    document.getElementById('info').innerHTML=this.getMarkerForPlayer(this.SET.plr);
  }

  /* generate dynamic game grid */
  generateGrid(){
    console.log('generate grid');
    for(var r=0;r<this.SET.rows;r++){
      this.SET.grid[r]=new Array();
      for(var c=0;c<this.SET.cols;c++){
        this.SET.grid[r][c]=this.SET.markerEmpty;
      }
    }
  }

  /* refresh grid */
  refreshGrid(){
    console.log('refresh grid');
    this.setGridHtml(this.generateGridHtml());
    this.generateHints();
  }

  /* set hints for possible moves */
  generateHints(){
    console.log('generate hints');
    // setup
    this.removeHints();
    if(this.playerHasStones()==false){
      // all are valid
      var regExLR='['+this.SET.markerEmpty+']{1}';
      var regExRL='['+this.SET.markerEmpty+']{1}';
      this.setHintsForRegEx(regExLR,regExRL);
    }else{
      // block: X_X
      var regExLR='['+this.getMarkerForPlayer(!this.SET.plr)+']{1}['+this.SET.markerValid+this.SET.markerEmpty+']{1}['+this.getMarkerForPlayer(!this.SET.plr)+']{1}';
      this.setHintsForRegEx(regExLR,regExLR);
      // block: XX_ / _XX
      var regExLR='['+this.SET.markerValid+this.SET.markerEmpty+']{1}['+this.getMarkerForPlayer(!this.SET.plr)+']{this.SET.stwin-1}';
      var regExRL='['+this.getMarkerForPlayer(!this.SET.plr)+']{this.SET.stwin-1}['+this.SET.markerValid+this.SET.markerEmpty+']{1}';
      this.setHintsForRegEx(regExLR,regExRL);
      // set: X_ / _X
      var regExLR='['+this.SET.markerValid+this.SET.markerEmpty+']{1}['+this.getMarkerForPlayer(this.SET.plr)+']{1}';
      var regExRL='['+this.getMarkerForPlayer(this.SET.plr)+']{1}['+this.SET.markerValid+this.SET.markerEmpty+']{1}';
      this.setHintsForRegEx(regExLR,regExRL);
    }
  }

  /* set valid marker according the given regex duo (LR,RL) */
  setHintsForRegEx(regExLR,regExRL){
    var pos=-1;
    // horizontal
    for(var r=0;r<this.SET.rows;r++){
      var line='';
      for(var c=0;c<this.SET.cols;c++){line+=this.SET.grid[r][c];}
      pos=this.getPositionForRegEx(line,regExLR,'x');
      if(pos>=0){this.setMarkerValidForRowAndCol(r,pos);}
      pos=this.getPositionForRegEx(line,regExLR,'>');
      if(pos>=0){this.setMarkerValidForRowAndCol(r,pos);}
      pos=this.getPositionForRegEx(line,regExRL,'<');
      if(pos>=0){this.setMarkerValidForRowAndCol(r,pos);}
    }
    // vertical
    for (c=0;c<this.SET.cols;c++){
      var line='';
      for(var r=0;r<this.SET.rows;r++){line+=this.SET.grid[r][c];}
      pos=this.getPositionForRegEx(line,regExLR,'x');
      if(pos>=0){this.setMarkerValidForRowAndCol(pos,c);}
      pos=this.getPositionForRegEx(line,regExLR,'>');
      if(pos>=0){this.setMarkerValidForRowAndCol(pos,c);}
      pos=this.getPositionForRegEx(line,regExRL,'<');
      if(pos>=0){this.setMarkerValidForRowAndCol(pos,c);}
    }
    // diagonal left-right-\
    if(this.SET.dng==true){
      var pr=0;
      var pc=this.SET.cols-1;
      while(pr<this.SET.rows){
        var p=0;
        var line='';
        while(parseInt(pc+p)<this.SET.cols&&parseInt(pr+p)<this.SET.rows){
          line+=this.SET.grid[parseInt(pr+p)][parseInt(pc+p)];p++;
        }
        pos=this.getPositionForRegEx(line,regExLR,'x');
        if(pos>=0){this.setMarkerValidForRowAndCol(parseInt(pr+pos),parseInt(pc+pos));}
        pos=this.getPositionForRegEx(line,regExLR,'>');
        if(pos>=0){this.setMarkerValidForRowAndCol(parseInt(pr+pos),parseInt(pc+pos));}
        pos=this.getPositionForRegEx(line,regExRL,'<');
        if(pos>=0){this.setMarkerValidForRowAndCol(parseInt(pr+pos),parseInt(pc+pos));}
        // count
        if(pc>-1){pc--;}
        if(pc==-1){pc=0;pr++;}
      }
    }
    // diagonal top-down-/
    if(this.SET.dng==true){
      var pr=0;
      var pc=0;
      while(pc<this.SET.cols){
        var p=0;
        var line='';
        while(parseInt(pc+p)<this.SET.cols&&parseInt(pr-p)>=0){
          line+=this.SET.grid[parseInt(pr-p)][parseInt(pc+p)];p++;
        }
        pos=this.getPositionForRegEx(line,regExLR,'x');
        if(pos>=0){this.setMarkerValidForRowAndCol(parseInt(pr-pos),parseInt(pc+pos));}
        pos=this.getPositionForRegEx(line,regExLR,'>');
        if(pos>=0){this.setMarkerValidForRowAndCol(parseInt(pr-pos),parseInt(pc+pos));}
        pos=this.getPositionForRegEx(line,regExRL,'<');
        if(pos>=0){this.setMarkerValidForRowAndCol(parseInt(pr-pos),parseInt(pc+pos));}
        // count
        if(pr<this.SET.rows){pr++;}
        if(pr==this.SET.rows){pr=this.SET.rows-1;pc++;}
      }
    }
  }

  /* set marker for valid move */
  setMarkerValidForRowAndCol(r,c){
    // int them
    r=parseInt(r);
    c=parseInt(c);
    // set marker and class
    if(r<this.SET.rows&&c<this.SET.cols){
      if(this.SET.grid[r][c]==this.SET.markerEmpty){
        this.SET.grid[r][c]=this.SET.markerValid;
        var boxid=this.TLS.getBoxIdForRowAndCol(r,c);
        this.TLS.addClassToElementById(boxid,'valid');
      }
    }
  }

  /* remove marker for valid move */
  removeMarkerValidForRowAndCol(r,c){
    // int them
    r=parseInt(r);
    c=parseInt(c);
    // set marker and class
    if(r<this.SET.rows&&c<this.SET.cols){
      if(this.SET.grid[r][c]==this.SET.markerValid){
        this.SET.grid[r][c]=this.SET.markerEmpty;
        var boxid=this.TLS.getBoxIdForRowAndCol(r,c);
        this.TLS.removeClassFromElementById(boxid,'valid');
      }
    }
  }

  /* remove all hints */
  removeHints(){
    for(var r=0;r<this.SET.rows;r++){
      for(var c=0;c<this.SET.cols;c++){
        if(this.SET.grid[r][c]==this.SET.markerValid){this.removeMarkerValidForRowAndCol(r,c);}
      }
    }
  }

  /* get amount of possible moves */
  getPossibleMovesAmount(){
    var all=document.querySelectorAll('div.box').length;
    var pl1=document.querySelectorAll('div.'+this.getColorNameForPlayer(this.SET.plr)).length;
    var pl2=document.querySelectorAll('div.'+this.getColorNameForPlayer(!this.SET.plr)).length;
    var amt=all-(pl1+pl2);
    return amt;
  }

  /* is grid empty */
  isGridEmpty(){
    var amt=document.querySelectorAll('div.box.'+this.getColorNameForPlayer(this.SET.plr)+',div.box.'+this.getColorNameForPlayer(!this.SET.plr)).length;
    if(amt==0){return true;}
    return false;
  }

  /* generate grid html */
  generateGridHtml(){
    console.log('generate grid html');
    // setup
    var html='';
    // dynamic grid
    for(var r=0;r<this.SET.rows;r++){
      for(var c=0;c<this.SET.cols;c++){
        // show marker
        var markstr='';
        if(this.SET.dbug==true||this.SET.grid[r][c]==this.SET.markerBlack||this.SET.grid[r][c]==this.SET.markerWhite){markstr=this.SET.grid[r][c];}
        // set colors (for counting) and borders
        var colcls='';
        if(this.SET.grid[r][c]==this.SET.markerBlack){colcls+=' '+this.getColorNameForPlayer(true);}
        if(this.SET.grid[r][c]==this.SET.markerWhite){colcls+=' '+this.getColorNameForPlayer(false);}
        if(r<this.SET.rows-1){colcls+=' boxbottom';}
        if(c<this.SET.cols-1){colcls+=' boxright';}
        // set box
        var boxid=this.TLS.getBoxIdForRowAndCol(r,c);
        html+='        <div class="box'+colcls+'" id="'+boxid+'" attr-row="'+r+'" attr-col="'+c+'">'+markstr+'</div>'+this.SET.nln;
      }
      // clear row
      html+='        <div class="clear"></div>'+this.SET.nln;
    }
    // return
    return html;
  }

  /* set grid html */
  setGridHtml(html){
    console.log('set grid html');
    document.getElementById('boxes').innerHTML=html;
  }

  /* next player: check possible moves (0:toggle back) */
  nextPlayer(){
    this.togglePlayer();
    this.generateHints();
    if(this.getPossibleMovesAmount()==0){
      this.togglePlayer();
      this.generateHints();
    }
    if(this.SET.dbug==true){this.refreshGrid()}
  }

  /* blink player info */
  blinkPlayerInfo(){
    if(this.SET.ovr==false){
      var cls=this.TLS.getClassStringForElementById('info');
      if(cls.includes('infodark')){
        this.TLS.removeClassFromElementById('info','infodark');
      }else{
        this.TLS.addClassToElementById('info','infodark');
      }
      setTimeout(()=>{this.blinkPlayerInfo();},1000);
    }else{
      this.TLS.removeClassFromElementById('info','infodark');
    }
  }

  /* player has stones on grid */
  playerHasStones(){
    var amt=document.querySelectorAll('div.box.'+this.getColorNameForPlayer(this.SET.plr)).length;
    if(amt==0){return false;}
    return true;
  }

  /* set stone for click */
  setStonesForClick(r,c){
    r=parseInt(r);
    c=parseInt(c);
    this.SET.grid[r][c]=this.getMarkerForPlayer(this.SET.plr);
  }

  /* did player win or not */
  checkForEndGame(){
    console.log('check for end game');
    // no possible moves for both players
    var plrend=0;
    for(var p=0;p<2;p++){
      if(this.getPossibleMovesAmount()==0){plrend++;}
      this.togglePlayer();
      this.generateHints();
    }
    // no possible moves for both players
    if(plrend==2){this.endGame();}
    // check for winning row
    var regEx='['+this.getMarkerForPlayer(this.SET.plr)+']{'+this.SET.stwin+'}';
    if(this.getPosForRegExSearchInGrid(regEx)!=''){this.endGame();}
  }

  /* mark winning row */
  markWinningRow(){
    // mark winning row
    var regExLR='['+this.getMarkerForPlayer(this.SET.plr)+']{'+this.SET.stwin+'}';
    var boxid='';
    var pos=-1;
    // horizontal
    for(var r=0;r<this.SET.rows;r++){
      var line='';
      for(var c=0;c<this.SET.cols;c++){line+=this.SET.grid[r][c];}
      pos=this.getPositionForRegEx(line,regExLR);
      if(pos>=0){
        for(var x=0;x<this.SET.stwin;x++){
          boxid=this.TLS.getBoxIdForRowAndCol(r,parseInt(pos+x));
          this.TLS.addClassToElementById(boxid,'win');
        }
        break;
      }
    }
    // vertical
    if(pos==-1){
      for(var c=0;c<this.SET.cols;c++){
        var line='';
        for(var r=0;r<this.SET.rows;r++){line+=this.SET.grid[r][c];}
        pos=this.getPositionForRegEx(line,regExLR);
        if(pos>=0){
          for(var x=0;x<this.SET.stwin;x++){
            boxid=this.TLS.getBoxIdForRowAndCol(parseInt(pos+x),c);
            this.TLS.addClassToElementById(boxid,'win');
          }
          break;
        }
      }
    }
    // diagonal left-right-\
    if(this.SET.dng==true&&pos==-1){
      var boxid='';
      var pr=0;
      var pc=this.SET.cols-1;
      while(pr<this.SET.rows){
        var p=0;
        var line='';
        while(parseInt(pc+p)<this.SET.cols&&parseInt(pr+p)<this.SET.rows){
          line+=this.SET.grid[parseInt(pr+p)][parseInt(pc+p)];p++;
        }
        pos=this.getPositionForRegEx(line,regExLR);
        if(pos>=0){
          for(var x=0;x<this.SET.stwin;x++){
            boxid=this.TLS.getBoxIdForRowAndCol(parseInt(pr+pos+x),parseInt(pc+pos+x));
            this.TLS.addClassToElementById(boxid,'win');
          }
          break;
        }
        // count
        if(pc>-1){pc--;}
        if(pc==-1){pc=0;pr++;}
      }
    }
    // diagonal top-down-/
    if(this.SET.dng==true&&pos==-1){
      var pr=0;
      var pc=0;
      while(pc<this.SET.cols){
        var p=0;
        var line='';
        while(parseInt(pc+p)<this.SET.cols&&parseInt(pr-p)>=0){
          line+=this.SET.grid[parseInt(pr-p)][parseInt(pc+p)];p++;
        }
        pos=this.getPositionForRegEx(line,regExLR);
        if(pos>=0){
          for(var x=0;x<this.SET.stwin;x++){
            boxid=this.TLS.getBoxIdForRowAndCol(parseInt(pr-pos-x),parseInt(pc+pos+x));
            this.TLS.addClassToElementById(boxid,'win');
          }
          break;
        }
        // count
        if(pr<this.SET.rows){pr++;}
        if(pr==this.SET.rows){pr=this.SET.rows-1;pc++;}
      }
    }
  }



  /* computer player is making a move */
  computerMove(){
    console.log('compuer move');
    // setup
    var boxid='';
    // aggressive (2), defensive (1), random (0)
    switch(this.SET.lvl) {
      case 2:
        boxid=this.computerMoveAggressive();
        break;
      case 1:
        boxid=this.computerMoveDefensive();
        break;
      case 0:
      default:
        boxid=this.computerMoveRandom();
    }
    // nothing found, do a random one
    if(boxid==''){boxid=this.computerMoveRandom();}
    // do click
    setTimeout(()=>{document.getElementById(boxid).click();},500);
  }

  /* defensive or aggressive differs just from the player flag */
  getBoxIdForDefensiveAndAggressiveMove(plrflag){
    // setup
    var boxid='';
    var pos='';
    // X_X
    var regExLR='['+this.getMarkerForPlayer(plrflag)+']{1}['+this.SET.markerValid+this.SET.markerEmpty+']{1}['+this.getMarkerForPlayer(plrflag)+']{1}';
    pos=this.getPosForRegExSearchInGrid(regExLR,'x');
    // XX_ / _XX
    if(pos==''){
      var regExLR='['+this.SET.markerValid+this.SET.markerEmpty+']{1}['+this.getMarkerForPlayer(plrflag)+']{'+this.getThresholdForLevel()+'}';
      var regExRL='['+this.getMarkerForPlayer(plrflag)+']{'+this.getThresholdForLevel()+'}['+this.SET.markerValid+this.SET.markerEmpty+']{1}';
      pos=this.getPosForRegExSearchInGrid(regExLR,'>');
      if(pos==''){pos=this.getPosForRegExSearchInGrid(regExRL,'<');}
    }
    // do we have a valid position inside the grid
    if(pos!=''){
      var crd=pos.split(',');
      if(crd[0]<this.SET.rows&&crd[1]<this.SET.cols){
        boxid=this.TLS.getBoxIdForRowAndCol(crd[0],crd[1]);
      }else{
        boxid=this.computerMoveRandom();
      }
    }
    // return
    return boxid;
  }

  /* try to do a defensive move */
  computerMoveDefensive(){
    return this.getBoxIdForDefensiveAndAggressiveMove(!this.SET.plr);
  }

  /* choose a move with an average (middle one) amount of turns */
  computerMoveAggressive(){
    return this.getBoxIdForDefensiveAndAggressiveMove(this.SET.plr);
  }

  /* choose a random move */
  computerMoveRandom(){
    // get possible moves
    var mvs=new Array();
    var elms=document.querySelectorAll('div.box:not(.'+this.getColorNameForPlayer(this.SET.plr)+'):not(.'+this.getColorNameForPlayer(!this.SET.plr)+')');
    elms.forEach((item)=>{
      mvs.push(parseInt(item.getAttribute('attr-row'))+','+item.getAttribute('attr-col'));
    });
    // choose a move, show which one and perform a click
    var which=Math.floor(Math.random()*mvs.length);
    var crd=mvs[which].split(',');
    var boxid=this.TLS.getBoxIdForRowAndCol(crd[0],crd[1]);
    return boxid;
  }



  /* init game */
  init(){
    console.log('init');
    console.log('cmp: '+this.SET.cmp);
    console.log('lvl: '+this.SET.lvl);
    // setup
    this.SET.grid=new Array();
    this.generateGrid();
    this.refreshGrid();
    this.SET.plr=false;
    this.nextPlayer();
    this.SET.ovr=false;
    // center wrapper
    this.TLS.centerWrapper();
    // hide cover
    this.TLS.hideCover();
    // timer for blinking
    setTimeout(()=>{this.blinkPlayerInfo();},500);
    // and go
    console.log('ready!');
  }



  /* end of game */
  endGame(){
    console.log('game ended');
    this.TLS.playSound(this.SND.erraud);
    this.SET.ovr=true;
    this.markWinningRow();
  }



  /* handle listener events */
  handleEvent(event) {
    event.preventDefault();
    switch(event.type) {
      case 'click':
        this.leftClickEvent(event);
        break;
      case 'keydown':
        this.keyEvent(event);
        break;
      case 'resize':
        this.resizeEvent(event);
        break;
    }
  }

   /* left mouse click */
   leftClickEvent(event){
    // clicked on a button
    if(event.target.matches('.button')){
      this.TLS.playSound(this.SND.btnaud);
      var action=event.target.getAttribute('attr-action');
      console.log(action);
      // what to do?
      if(this.SET.ovr==true&&action=='new'){this.init();}
      if(this.SET.ovr==false&&action=='hnt'){this.SET.dbug=!this.SET.dbug;this.refreshGrid();}
    }
    // clicked on a box
    if(this.SET.ovr==false&&event.target.matches('.box')){
      this.TLS.playSound(this.SND.clkaud);
      var r=event.target.getAttribute('attr-row');
      var c=event.target.getAttribute('attr-col');
      // set stones, refresh grid, check, switch player
      if(this.SET.grid[r][c]==this.SET.markerValid||this.SET.grid[r][c]==this.SET.markerEmpty){
        // set stone and refresh
        this.setStonesForClick(r,c);
        this.refreshGrid();
        // check if game is over
        this.checkForEndGame();
        // toggle player
        if(this.SET.ovr==false){this.nextPlayer();}
        // white (false) can be a computer player
        if(this.SET.ovr==false&&this.SET.cmp==true&&this.SET.plr==false){this.computerMove();}
      }
    }
  }

   /* key press event */
  keyEvent(event){
    switch(event.code){
    // n(ew)
    case 'KeyN':
      if(this.SET.ovr==true){this.TLS.clickOnElementById('new');}
      break;
    // h(elp)
    case 'KeyH':
      if(this.SET.ovr==false){this.TLS.clickOnElementById('hnt');}
      break;
    // r(eset)
    case 'KeyR':
      this.init();
      break;
    }
  }

  /* window resize event */
  resizeEvent(event){
    // restart game
    this.init();
  }
}



/* wait until document is fully loaded and ready to start */
let stateCheck=setInterval(()=>{
  if(document.readyState=='complete'){
    clearInterval(stateCheck);
    GME=new Game();
    GME.init();
  }
},250);